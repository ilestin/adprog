package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;

import java.util.List;
import java.util.ListIterator;

public class MacroCommand implements Command { //macro command utk ngehandle masing masing device

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands); //ngambil command dari list ke macrocommand
    }

    @Override
    public void execute() {
        // TODO Complete me!
        for (int j = 0; j < commands.size(); j++) {
            commands.get(j).execute();
        }
    }

    @Override
    public void undo() {
        // TODO Complete me!
        for (int j = commands.size() - 1; j >= 0; j--) {
            commands.get(j).undo();
        }
    }
}
