package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

public interface Command {

    void execute(); //ibaratkan orderUp()

    void undo();
}
