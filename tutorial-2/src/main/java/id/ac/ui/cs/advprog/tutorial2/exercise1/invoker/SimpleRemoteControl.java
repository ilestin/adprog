package id.ac.ui.cs.advprog.tutorial2.exercise1.invoker;

import id.ac.ui.cs.advprog.tutorial2.exercise1.command.Command;

public class SimpleRemoteControl {

    private Command slot; //1 slot utk kontrol 1 device

    public void setCommand(Command command) {
        // TODO Complete me!
        slot = command; //kalo mau ganti perilaku si remote button
    }

    public void buttonWasPressed() {
        // TODO Complete me!
        slot.execute(); //ketika button dipencet
    }
}
