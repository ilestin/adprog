package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    //membuat 2 reference behavior interface
    //semua subclass duck akan meng-inherit ke dua hehavior
    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() { //to replace fly
        flyBehavior.fly();
    }

    public void performQuack() { //to replace quack
        quackBehavior.quack();
    }

    // TODO Complete me!
    public abstract void display();

    public void swim () {
        System.out.println("All ducks float, even decoys!");
    }

    public void setFlyBehavior(FlyBehavior fb) {
        flyBehavior = fb;
    }

    public void setQuackBehavior(QuackBehavior qb) {
        quackBehavior = qb;
    }

}
